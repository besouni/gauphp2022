<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Lecture _ 2</title>
</head>
<body>
      <?php
         $m6 = ["person1"=>"Davit", "person2"=>"Davit", "person1"=>"Ana"];
         $m7 = ["person1"=>"Davit", "ana", "eka", "belka", "person2"=>"bana", "gana"];
         $m8 = ["person1"=>["Ana", "Dvali", 23, "Web developer"], "person2"=>["Tengo", "Kakulia"]]
      ?>
       <pre>
         <?php
            print_r($m7);
            echo "<br> associaciuri masivi<br>";
            print_r($m8);
         ?>
      </pre>
      <hr><hr>
      <?php
         $m5 = [
            [3, 4, 5, 9],
            [3, "php"],
            [true, 8, 9.7]
         ]
      ?>
       <pre>
         <?php
            print_r($m5);
         ?>
      </pre>
      <hr><hr>
      <?php
         $m1 = [23, 94.9, 394, "Beso", false];
         echo  rand(10, 100);
         $m2 = array(4, 55, 4, "Hello");
         $m3 = [];
         $m4 = array();
      ?>
      <pre>
         <?php
            print_r($m1);
            var_dump($m1);
         ?>
      </pre>
      <div>
         <h3>for</h3>
         <?php
            for($i=0; $i<count($m2); $i++){
               echo $m2[$i]."<br>";
            }
         ?>
      </div>
      <div>
         <h3>for</h3>
         <?php
            foreach($m1 as $k => $el){
               echo $k." - ".$el."<br>";
            }
         ?>
      </div>
</body>
</html>