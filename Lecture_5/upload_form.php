<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Upload Form</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
      <div class="container">
         <?php
            if(date("Y-m-d H:i:s") > "2022-04-12 16:09:00" && date("Y-m-d H:i:s") < "2022-04-15 16:09:39"){
               print_r(date("Y-m-d H:i:s"));
         ?>
         <form method="post" enctype="multipart/form-data">
            Select image to upload:
            <input type="file" name="fileToUpload">
            <input type="submit" value="Upload File" name="upload">
         </form>
         <?php
            }
         ?>
      </div>
      <div class="container">
         <?php
            include "upload.php";
         ?>
      </div>
</body>
</html>