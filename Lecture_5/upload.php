<?php
   $extension_error = "";
   $size_error = "";
   $success_message = "";

   if(isset($_POST["upload"])) {
      $file_size = $_FILES["fileToUpload"]["size"];
      $file_extension = strtolower(pathinfo($_FILES["fileToUpload"]["name"], PATHINFO_EXTENSION));
      
      if($file_extension!="jpg" && $file_extension!="txt"){
         $extension_error =  "Wrong Extension!!!";
      }
      
      if($file_size > 5000 && $file_size > 1000){
         $size_error =  "Wrong Size!!!";
      }
      
      if($extension_error == "" && $size_error == ""){
         move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "uploads/".time().$_FILES["fileToUpload"]["name"]); 
         $success_message = "File uploaded";
      }      
      
      // echo "<pre>";
      //    print_r($_FILES);
      // echo "</pre>";
      // $fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

      // echo $_FILES["fileToUpload"]["name"];
      // echo "<hr>";
      // echo "<hr>";
      // print_r(strtolower(pathinfo($_FILES["fileToUpload"]["name"], PATHINFO_EXTENSION)));
      // echo "<hr>";
      // echo "<hr>";
    }

    echo $extension_error;
    echo "<br>";
    echo $size_error;
    echo "<br>";
    echo $success_message;
?>