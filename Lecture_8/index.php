<?php
   require_once "connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
   <div class="main">
      <div class="nav">
         <ul>
            <li><a href="index.php">HOME</a></li>
            <li><a href="?menu=select">SELECT</a></li>
            <li><a href="?menu=insert">INSERT</a></li>
            <!--<li><a href="?menu=update">UPDATE</a></li>
            <li><a href="?menu=delete">DELETE</a></li>-->
         </ul>
      </div>
      <div class="content">
         <?php
            if( (isset($_GET["menu"]) && $_GET["menu"]=="insert") || isset($_POST['insert'])){
               include "menu/insert.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="select"){
               include "menu/select.php";
            }else if(isset($_GET["change"]) && $_GET["change"]=="edit"){
                include "menu/edit.php";
            }
         ?>
      </div>
   </div>
</body>
</html>